# ASSIGNMENT 2
Imagine a server with the following specs:
- 4 times Intel(R) Xeon(R) CPU E7-4830 v4 @ 2.00GHz
- 64GB of ram
- 2 tb HDD disk space
- 2 x 10Gbit/s nics

The server is used for SSL offloading and proxies around 25000 requests per second.

Please let us know which metrics are interesting to monitor in that specific case and how would you do that?  What are the challenges of monitoring this?

# Solution
### The metrics are used to monitoring:
| Name | Description |
| ----------- | ----------- |
| Rate | The number of requests per second |
| Errors | The rate of requests that fail |
| Latency | The time it takes to service a request |
| Saturation | How “full” the service is. How much more load can it handle?  |
| Utilization | How busy the resource or system is |
| SSL Certificate Expiry | Tracks certificate expiration dates |

TLS operations consume extra CPU resources. The most CPU-intensive operation is the TLS handshake, also TLS record will add network overhead. So I would like to focus monitoring server CPU and network metrics with [USE](http://www.brendangregg.com/usemethod.html) method. For application monitoring I will use [RED](https://grafana.com/files/grafanacon_eu_2018/Tom_Wilkie_GrafanaCon_EU_2018.pdf) method.

Beside monitoring metrics, we also need to monitor logs. We tend to use logs to find the root cause of an issue, as the information we need is often not available as a metric. E.g we can get HTTP status code from the access logs.

In my opinion, with the following server specs, look like it's overused. With 2 CPU cores and 4 GB RAM, Nginx can achive [90000](https://www.nginx.com/wp-content/files/nginx-pdfs/Sizing-Guide-for-Deploying-NGINX-on-Bare-Metal-Servers.pdf) requests/sec. In case of using the server as a proxy as well as caching we should replace HDD disk with SSD disk to get better performance.

### How to setup monitoring:
- Setup Prometheus, Grafana, AlertManager and ELK or Graylog
- Using [Prometheus node-exporter](https://github.com/prometheus/node_exporter) to export hardware and OS metrics
- Using [HAProxy exporter](https://github.com/prometheus/haproxy_exporter) or [Nginx VTS exporter](https://github.com/hnlq715/nginx-vts-exporter) to export application metrics
- Using [ssl exporter](https://github.com/ribbybibby/ssl_exporter) to export certificate information
- Capture system and application logs and ship into ELK or Graylog
- Build dashboards
- Setup alert rules

### The challenges of monitoring
- Figure out important metrics to monitor
- Metrics monitoring tool should not effect the server's performance
- Build meaningful dashboards
- Set proper trigger warning value to minimize false alarm
- Combination metrics and logs to detect and trace root cause
- Deploy monitoring configuration as code

### TLS performance checklist:
- Tuning OS and TCP
- Upgrade TLS libraries to latest release
- Enable and configure session caching and stateless resumption
- Monitor session caching hit rates and adjust configuration accordingly
- Enable TLS False Start
- Terminate TLS sessions closer to the user to minimize roundtrip latencies
- Use dynamic TLS record sizing to optimize latency and throughput
- Audit and optimize the size of your certificate chain
- Configure OCSP stapling
- Configure HSTS and HPKP
- Configure CSP policies
- Enable HTTP/2
