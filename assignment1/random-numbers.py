#!/usr/bin/env python3

import random

def main():
  # Generate list of numbers from 1 to 10
  list_numbers = [number for number in range(1, 11)]
  # Shuffle the order of numbers in the list
  random.shuffle(list_numbers)
  print("The numbers in random order:", list_numbers)

if __name__ == '__main__':
  main()
