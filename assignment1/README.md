# ASSIGNMENT 1
Please write a simple CLI application in the language of your choice that does the following: Print the numbers from 1 to 10 in random order to the terminal.

Please provide a README, that explains detailed how to run the program on MacOS and Linux.

# Solution
## Without Docker
### Prerequisites
We need Python 3 on MacOS and Linux

### MacOS:
We can install Python 3 by using **Homebrew**
#### Steps:
- Install XCode:
```
$ xcode-select --install
```
- Install Homebrew:
```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```
- Install Python 3:
```
$ brew install python3
```
- Check Python version:
```
$ python3 --version
```

### Linux:
If using Ubuntu, recommend using the **deadsnakes PPA** to install Python 3:
```
$ sudo apt-get install software-properties-common
$ sudo add-apt-repository ppa:deadsnakes/ppa
$ sudo apt-get update
$ sudo apt-get install python3.8
```
If using other Linux distributions, we may have Python 3 pre-installed as well. If not, using the distribution’s package manager. For example on Fedora, we would use `dnf`:
```
$ sudo dnf install python3
```

### Run the script:
```
python3 random-numbers.py
```

## With Docker
We can run the script on Docker instead of installing Python 3. This is applying for both MacOS and Linux
- Install Docker: https://docs.docker.com/engine/install/
- Run the python script on Docker
```
$ docker run -it --rm \
       --name random-numbers \
       -v "$PWD":/app \
       -w /app \
       python:3.8-alpine \
       python random-numbers.py
```
- Or run the bash script on Docker
```
$ docker run -it --rm \
       --name random-numbers \
       -v "$PWD":/app \
       -w /app \
       busybox \
       sh random-numbers.sh
```
